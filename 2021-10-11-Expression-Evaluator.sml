(* Q1 - Capturing the expressions *)

datatype Expr = Const of real | Plus of Expr * Expr | Mult of Expr | Var of string

(* Q2 - Capturing the statements in Language *)
									
datatype Stmt = Assignment of Expr * string | Print of Expr

(* helper functions for Q3 and Q4 *)

type s = real option * real option
