(* 1 *)
type var = string;
datatype expr = variable of var
                | application of expr * expr
                                | Abstraction of var * expr;
datatype l_let = let_variable of var
                | let_application of l_let * l_let
                             | let_Abstraction of var * l_let
                                          | let_lambda of var * l_let * l_let;
 
(* 2 *)                                          
fun unlet (let_variable(v))               = variable(v)
  | unlet (let_application(expr1, expr2)) = application(unlet(expr1), unlet(expr2))
    | unlet (let_Abstraction(v, expr1))     = Abstraction(v, unlet(expr1))
      | unlet (let_lambda(v, expr1, expr2))   = application(Abstraction(v, unlet(expr2)), unlet(expr1));

