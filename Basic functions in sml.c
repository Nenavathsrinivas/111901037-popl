Basic functions in SML
Deadline: 26th Aug 2020, 23:59 hrs
In this exercise you do some basic functions


1. Write the tr1i-variate (i.e. 3 variable) versions of curry and
uncurry functions.  First step is to write down the type (in the
comments).
Ans. The curry function with tri-variate:
fun curry f x y z = f(x,y,z); # val curry = fn : ('a * 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd  
The curried version of function with tri-variate:
fun uncurry f(x,y,z) = f x y z; # val uncurry = fn :('a -> 'b -> 'c -> 'd ) -> 'a * 'b * 'c -> 'd      
2. Write the functions fst : 'a * 'b -> 'a and snd : 'a * 'b -> 'b
that project a tuple into its components.
Ans.fst function:- 
fun fst (a,b) = a;
(* val fst = fn : 'a * 'b -> 'a*)
snd function:-
fun snd (a,b) = b;
(* val snd = fn : 'a * 'b -> 'b *)
3. Write the length function for lists length : 'a list -> int.
Ans. fun length(x)
     = if null(x) then 0
       else 1+length(t1(x));
     (* val length = fn : 'a list -> int *)
    Checking Output:-
    val x = [1,2,3,4,5];
    length(x);

4.Write the reverse : 'a list -> 'a list function. Be careful to
not make it O(n^2)
Ans. fun reverse [ ] = [ ]
  |   reverse (x::xs) = reverse xs @ [x]

5.Write a function to compute the nth element in the Fibonacci
sequence fib : int -> int. Be careful for the obvious version is
exponential.
Ans. fun fib n =
    let fun fibo 0 a b = a
          | fibo 1 a b = b
          | fibo n a b = fibo (n-1) b (a+b)
