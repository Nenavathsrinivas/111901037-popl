Lists and Trees
In this assignment we look at functions on lists and binary trees.

1. For lists define the function map : ('a -> 'b) -> 'a list -> 'b list. The semantics of map is that it applies the given function
on all the elements of the list, i.e.
 map f [x1, x2,....,xn] = [f x1, f x2, ... f xn]
Ans. fun map f [] = []
    | map f (x::xs) = f x :: map f xs;

2. Define the data type 'a tree that captures a binary tree.
Ans. datatype 'a tree = Null | Cons of 'a tree * 'a * 'a tree;

3. First write its type and then complete its definition.
Ans. fun treemap f Null = Null| treemap f (Cons (l,root, r)) = Cons (treemap f l, f root, treemap f r);

4. Define the in-order, pre-order and post-order traversal of the
binary tree returning the list of nodes in the given order. First
write down the type of the function(s) and then go about defining
them.
Ans. fun preorder Null  = [] | preorder (Cons (l,root,r))=[root] @ preorder l @ preorder r;
fun inorder Null = [] | inorder(Cons(l,root,r))=inorder l @ [root] @ inorder r;
fun postorder Null = [] | postorder(Cons(l,root,r))=postorder l @ postorder r @ [root];


5. Define the rotate clockwise function for binary trees. Pictorially
this rotation function is defined as the following.
            a                   b
           / \                 / \
          / ⭮ \               /   \
         b    🌲₃  ======>   🌲₁   a
        / \                       / \
       /   \                     /   \
      🌲₁   🌲₂                 🌲₂    🌲₃
If the left subtree of the root is null then rotation is identity operation.
Ans. fun rotate Null = Null
    | rotate (Cons(Null,root,r))=(Cons(Null,root,r))
    | rotate (Cons(Cons(l',root',r'),root,r))=(Cons (l',root',Cons(r',root,r)));