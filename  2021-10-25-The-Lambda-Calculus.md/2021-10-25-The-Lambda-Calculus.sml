The Lambda calculus : A small functional language
Deadline: 1st November 2021 23:59.
We look at the following simple functional programming language whose
abstract syntax is given as follows.
<expr>  := variable                    -- A variable like x, y, z
        | <expr> <expr>                -- e₁ e₂ should mean the function e₁ applied on e₂ (like sml)
		| "fn" variable "=>" <expr>    -- fn x => e  means that function that takes x to e

Examples of programs in this language are
fn x => x  – The indentity function
fn x y => x – The const function etc.
Note: We will not need parenthesis in the AST but when we want to
express programs in this language as a string we will need to put
brackets to disambiguate the application order.


1) Define a type expr to capture this abstract syntax using ML data
types with variables represented as strings
    type var      = string
    datatype expr = ...
Ans)  type var = string
     datatype expr = var|comb of
     expr*expr|lamb of var*expr


2) Consider an expression like fn x => x y. Here x is a bound
variable because it occurs in the shadow of the binder fn x => ... However, y is a free variable. Write a functions free : expr -> var list to compute the list of free and all the
Hint: First write down a formal definition for what is the set of
free variable. We know that there are only three kinds of expressions

1) A variable x

2) A function application e₁ e₂

3) An function abstraction fn x => e


In each of these case what is the set of free variables ? Write SML
functions that compute the free, bound and all variables of a given
lambda calculus expression.
Ans) type var = string
datatype expr = vari of var | comb of expr*expr | lamb of var*expr

fun delete (a,[]) = []
  | delete (a,x::xs) = if a = x then delete(a,xs)
    else x::delete(a,xs)

fun free (e:expr) = case e of
   vari x => [x]
        | comb (e1,e2) => free e1 @ free e2
        | lamb (y, e3) => delete (y, free e3)

You can use the standard ML library set implementation.
(*
Define a structure of type ORD_KEY for your variables (which is string or you can use Atom)

In case you are using Atom.atom for your varialbes AtomSet already gives you an implementation
of sets over Atom.atom. You can use that.

https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/ord-key-sig.html

*)

structure StringKey : ORD_KEY = struct
 ...
 ...

end (* structure StringKey *)


structure StringSet = RedBlackSetFn (StringKey) (* Can use this for string set implementation *)



3) Write a function subst : var * expr -> expr -> expr where subst (x,N) M substitutes all free occurrence of x in M with N. In
mathematical notation it is written as M [x:=N].
Ans) type var = string 
datatype expr = vari of var | comb of expr*expr | lamb of var*expr

fun subst (y,N:expr) (vari(a)) = if (a = y) then N else (vari(a))
  | subst (y,N:expr) (comb(a,b)) = comb((subst (y,N) a), (subst (y,N) b))
  | subst (y,N:expr) (lamb(a,b)) = if (y=a) then (lamb(a,b)) else (lamb(a,(subst (y,N) b)))
